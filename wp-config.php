<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'websitesekolah' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ql*tv?hF!MY~hAdv${*v<.Q=5o%pvmHW!u1S1tuf&RDa./[eaedN3.ki`>qQ_qiX' );
define( 'SECURE_AUTH_KEY',  'd$N5yKz`~5Za85Uls;6$[^O#bZ:v-MMG@nnFhu?1GL-pEZ?tV8qt;7oTX%HRgAbP' );
define( 'LOGGED_IN_KEY',    'em:ejC.ns)(u]L-pb=kkca}X.QGz.R24CpP%@&s1IOE0DkdvH!2cP$rCl:*lYqqh' );
define( 'NONCE_KEY',        'EoD_8GcpJiNu.t|X6SS;kkH/p$zsO5NtWT`}tY1PdO4jG9sjaHA}K@2sO{AQu}lh' );
define( 'AUTH_SALT',        'zx<+iICte7Ph  s;WR%mqVsp<7#nu6*e82??~LNpgd^)hu3hBY(d7QZ2&`s9Ab4!' );
define( 'SECURE_AUTH_SALT', 'cI0]kygI/k/,|BmZ00J<KP`&/;$z-8^$Uw8BQ*++nc`-/NpMik,@xn8r(3qb]!f ' );
define( 'LOGGED_IN_SALT',   'ClN7,oyf]RG^7R{{J:8;{f,,jCvsU2x|6Mr40^$$z#NCqB$&x!V%EKyDd,F(XJnd' );
define( 'NONCE_SALT',       'lPM<;|qv6et }8xqLLp]Z}g?9b4<5]/*+.NTpi}-5X[lK7f|,0e?3&AU`,Ad1)S1' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
